const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/db');

const UserSchema = mongoose.Schema({
  name: {
    type: String,
  },
  email: {
    type: String,
    required: true,
  },
  login: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

const User = module.exports = mongoose.model('User', UserSchema);
// Создаём объект, который будем экспортировать, и этот объект
// построен на схеме UserSchema

/**
 * Далее необходимо прописать функции для работы с моделью,
 * которые позволя, например, добавить пользователя.
 */

module.exports.getUserByLogin = function(login, callback) {
  const query = {login: login};
  User.findOne(query, callback);
};

module.exports.getUserById = function(id, callback) {
  User.findById(id, callback);
};

module.exports.addUser = function(newUser, callback) {
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if (err) throw err;
      newUser.password = hash;
      newUser.save(callback);
    });
  });
  //newUser.save(callback);
};

module.exports.comparePass = function(passFromUser, userDBPass, callback) {
  bcrypt.compare(passFromUser, userDBPass, (err, isMatch) => {
    if (err) throw err;
    callback(null, isMatch);
  });
};