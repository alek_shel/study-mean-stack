import { Component, OnInit } from '@angular/core';
import { CheckFormService } from '../check-form.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reg',
  templateUrl: './reg.component.html',
  styleUrls: ['./reg.component.css']
})
export class RegComponent implements OnInit {

  name: string;
  login: string;
  email: string;
  password: string;

  constructor(
    private checkForm: CheckFormService,
    private flashMessages: FlashMessagesService,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
  }

  userRegisterClick(): boolean {
    const user = {
      name: this.name,
      email: this.email,
      login: this.login,
      password: this.password,
    };

    if (!this.checkForm.checkName(user.name)) {
      this.flashMessages.show('Имя пользователя не введено', {
        cssClass: 'alert-danger',
        timeout: 4000
      });
      return false;
    }

    if (!this.checkForm.checkName(user.login)) {
      this.flashMessages.show('Логин не введен', {
        cssClass: 'alert-danger',
        timeout: 4000
      });
      return false;
    }

    if (!this.checkForm.checkName(user.email)) {
      this.flashMessages.show('Email не введен', {
        cssClass: 'alert-danger',
        timeout: 4000
      });
      return false;
    }

    if (!this.checkForm.checkName(user.password)) {
      this.flashMessages.show('Пароль не введен', {
        cssClass: 'alert-danger',
        timeout: 4000
      });
      return false;
    }

    this.authService.registerUser(user).subscribe(data => {
      if (!data.success) {
        this.flashMessages.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 4000
        });
        this.router.navigate(['/reg']);
      }

      this.flashMessages.show(data.msg, {
        cssClass: 'alert-success',
        timeout: 2000
      });
      this.router.navigate(['/auth']);
    });
  }

}
