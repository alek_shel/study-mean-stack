import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CheckFormService {

  constructor() { }

  checkName(name: string): boolean {
    if (name === undefined) {
      return false;
    }
    return true;
  }

  checkEmail(email: string): boolean {
    if (email === undefined) {
      return false;
    }
    return true;
  }

  checkLogin(login: string): boolean {
    if (login === undefined) {
      return false;
    }
    return true;
  }

  checkPassword(password: string): boolean {
    if (password === undefined) {
      return false;
    }
    return true;
  }
}
